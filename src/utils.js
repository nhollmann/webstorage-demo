import { useEffect, useState } from 'react';

/**
 * React Hook zum erzwingen eines Updates.
 */
export const useForceUpdate = () => {
    const [, setValue] = useState(0);
    return () => setValue(value => ++value);
};

/**
 * React Hook um ein Callback an das Storage Event zu binden.
 * 
 * @param {Function} callback 
 */
export const useStorageEvent = callback => {
    useEffect(() => {
        window.addEventListener("storage", callback);
        return () => window.removeEventListener("storage", callback);
    }, [callback]);
};
