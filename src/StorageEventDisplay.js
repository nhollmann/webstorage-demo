import React, { useState, useCallback } from 'react';
import Button from 'react-bootstrap/Button';
import { useStorageEvent } from './utils'

/**
 * StorageEventDisplay Komponente.
 * Zeigt einen Log mit allen Storage Events an.
 */
function StorageEventDisplay() {
    const [messageList, setMessageList] = useState("");

    const eventCallback = useCallback((e) => {
        let output = "";

        output += "Schlüssel: " + e.key + "\n";

        if (e.oldValue != null) {
            output += "Alter Wert: \"" + e.oldValue + "\"\n";
        } else {
            output += "Alter Wert: null\n";
        }

        if (e.newValue != null) {
            output += "Neuer Wert: \"" + e.newValue + "\"\n";
        } else {
            output += "Neuer Wert: null\n";
        }
        output += "URL: " + e.url + "\n";

        setMessageList((old) => old + "\n" + output);
    }, []);

    useStorageEvent(eventCallback);

    return (
        <div>
            <pre>
                <code>
                    {messageList}
                </code>
            </pre>
            <br/>
            <Button variant="danger" onClick={() => setMessageList("")}>Leeren</Button>
            <br/>
        </div>
    );
}

export default StorageEventDisplay;
