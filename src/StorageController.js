import React, { useState, useCallback } from 'react';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import ListGroup from 'react-bootstrap/ListGroup';
import { useStorageEvent, useForceUpdate } from './utils';

/**
 * StorageController Komponente.
 * Zeigt alle Einträge eines Storages an und erlaubt es diese zu Löschen oder
 * neue Hinzuzufügen.
 * 
 * @param {Object} props Properties 
 * @param {Storage} props.storage Der zu verwendende Storage 
 */
function StorageController(props) {
    const [key, setKey] = useState("");
    const [value, setValue] = useState("");
    const forceUpdate = useForceUpdate();

    const eventCallback = useCallback((e) => {
        if (props.storage === e.storageArea) {
            forceUpdate();
        }
    }, [props.storage, forceUpdate]);

    useStorageEvent(eventCallback);

    /**
     * Einen neuen Eintrag abspeichern.
     */
    const insertKey = () => {
        props.storage.setItem(key, value);
        setValue("");
        forceUpdate();
    };

    /**
     * Alle Einträge löschen.
     */
    const deleteAll = () => {
        props.storage.clear();
        forceUpdate();
    };

    /**
     * Den Eintrag mit dem Schlüssel k löschen.
     * 
     * @param {string} k der zu löschende Schlüssel
     */
    const deleteEntry = (k) => {
        props.storage.removeItem(k);
        forceUpdate();
    };

    let keys = [];
    for (let i = 0; i < props.storage.length; i++) {
        keys.push(props.storage.key(i));
    }

    const entries = keys.map(k => {
        return {
            key: k,
            value: props.storage.getItem(k),
        };
    });

    return (
        <div>
            <p>
                Anzahl Einträge: {props.storage.length}
            </p>

            <ListGroup>
                {entries.map(entry => (
                    <ListGroup.Item
                        key={entry.key}
                        action
                        variant="info"
                        onClick={() => deleteEntry(entry.key)}
                    >
                        {entry.key}: "{entry.value}"
                    </ListGroup.Item>
                ))}
            </ListGroup>
            <br />

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">Schlüssel</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    placeholder="Schlüssel"
                    aria-label="Schlüssel"
                    aria-describedby="basic-addon1"
                    onChange={e => setKey(e.target.value)}
                    value={key}
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">Wert</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    placeholder="Wert"
                    aria-label="Wert"
                    aria-describedby="basic-addon1"
                    onChange={e => setValue(e.target.value)}
                    value={value}
                />
            </InputGroup>
            <ButtonToolbar className="justify-content-between">
                <Button variant="success" onClick={insertKey}>Eintrag einfügen</Button>
                <Button variant="danger" onClick={deleteAll}>Daten löschen</Button>
            </ButtonToolbar>
        </div>
    );
}

export default StorageController;
