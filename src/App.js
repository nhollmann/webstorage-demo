import React from 'react';
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import StorageController from './StorageController';
import StorageEventDisplay from './StorageEventDisplay';

/**
 * App Komponente. 
 * Setzt die gesammte Applikation zusammen.
 */
function App() {
  return (
    <Container>
      <Jumbotron fluid>
        <Container>
          <h1>WebStorage API Demo</h1>
          <p>
            Interaktive Demo zur WebStorage API, Seminar Web-Anwendungen von Nicolas Hollmann (cgt103016).
          </p>
        </Container>
      </Jumbotron>

      <Container>
        <Row>
          <Col xs={12} md={6}>
            <h2>localStorage</h2>
            <StorageController storage={window.localStorage} />
          </Col>
          <Col xs={12} md={6}>
            <h2>sessionStorage</h2>
            <StorageController storage={window.sessionStorage} />
          </Col>
        </Row>
        <br/>
        <Row>
          <Col>
            <h2>Storage Event</h2>
            <StorageEventDisplay />
          </Col>
        </Row>
      </Container>
      <br/>
    </Container>
  );
}

export default App;
