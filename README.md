# WebStorage API Demo

Dies ist die begleitende Demo zu meiner Präsentation "WebStorage API" an der FH Wedel.
Die laufende App kann unter [https://nhollmann.gitlab.io/webstorage-demo/](https://nhollmann.gitlab.io/webstorage-demo/)
betrachtet und ausprobiert werden.

## Skripte

Im Projektverzeichnis können folgende Befehle ausgeführt werden:

### `yarn dev`

Startet die App im Entwicklungsmodus.<br />
Öffne [http://localhost:3005](http://localhost:3005) um es im Browser anzuzeigen.

Die Seite wird bei Änderungen am Code neu geladen.

### `yarn build`

Baut die App für die Produktion in den `build` Ordner.<br/>
Dabei wird React gebundelt und die App für die beste Performance optimiert.<br/>
Die App kann jetzt verwendet werden.

### `yarn start`

**Zuvor muss `yarn build` aufgerufen worden sein.**

Startet einen Demo-Server auf [http://localhost:3005](http://localhost:3005).
